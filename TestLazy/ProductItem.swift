//
//  ProductItem.swift
//  TestLazy
//
//  Created by baophan on 7/26/15.
//  Copyright (c) 2015 @baophan94. All rights reserved.
//

import UIKit

class ProductItem: NSObject {
	var name: String!
	var sku: Int!
}
