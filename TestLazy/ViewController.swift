//
//  ViewController.swift
//  TestLazy
//
//  Created by baophan on 7/26/15.
//  Copyright (c) 2015 @baophan94. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var myTableView: UITableView!
	
	// TEST 1: UNCOMMENT THIS:
	 var allProducts: [ProductItem] = ViewController.addProducts()
	
	// TEST 2: UNCOMMENT THIS:
	// lazy var allProducts: [ProductItem] = self.addProducts()
	
    override func viewDidLoad() {
        super.viewDidLoad()

		myTableView.registerNib(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
		myTableView.delegate = self
		myTableView.dataSource = self
		
    }
	
	
	
	// FOR TEST 1: UNCOMMENT THIS:
	
	internal static func addProducts() -> [ProductItem] {
		var allItems = [ProductItem]()
		while count(allItems) < 100 {
			var item = ProductItem()
			item.name = "My Product \(count(allItems))"
			item.sku = count(allItems)
			allItems.append(item)
		}
		return allItems
	}

	
	// FOR TEST 2: UNCOMMENT THIS:
	/*
	func addProducts() -> [ProductItem] {
		var allItems = [ProductItem]()
		while count(allItems) < 100 {
			var item = ProductItem()
			item.name = "My Product \(count(allItems))"
			item.sku = count(allItems)
			allItems.append(item)
		}
		return allItems
	}
	*/

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return allProducts.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell: TableViewCell = (tableView.dequeueReusableCellWithIdentifier("TableViewCell", forIndexPath: indexPath) as? TableViewCell)!
		
		cell.myCell.text = allProducts[indexPath.row].name
		
		return cell
	}
	
	func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return true
	}
	
	func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		if editingStyle == .Delete {
			allProducts.removeAtIndex(indexPath.row)
			myTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
		}
	}

}
